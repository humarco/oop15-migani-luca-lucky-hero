package org.lkyhro.item;

/**
 * Created by Migani Luca on 08/02/2016.
 */
public enum ItemType {

    HELM, BOOTS, GLOVES, ARMOR, SHIELD, WEAPON, HEALING, THROWABLE

}
